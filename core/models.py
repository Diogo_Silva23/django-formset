from django.db import models

#simple model to show relation manytomany 

class Actor(models.Model):
    firs_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)    
    age = models.SmallIntegerField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{0} {1}".format(self.firs_name, self.last_name)

class Movies(models.Model):
    title = models.CharField(max_length=255)
    actors = models.ManyToManyField(Actor)
    created  = models.DateTimeField(auto_now_add=True)
    updated = models.DateField(auto_now=True)

    def __str__(self):
        return self.title

