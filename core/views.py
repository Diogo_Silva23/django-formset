from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView
from core.models import *

class Index(TemplateView):
    template_name = 'index.html'

class MovieCreateView(CreateView):
    model = Movies
    fields = '__all__'
    template_name = 'core/movie_form.html'

class ActorCreateView(CreateView):
    model = Actor
    fields = '__all__'
    template_name = 'core/actor_form.html'
    success_url = reverse_lazy('index')